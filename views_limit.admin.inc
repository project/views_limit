<?php

/**
 * @file
 * Admin page callbacks for the View Limit module.
 *
 * 
 */
function views_limit_setting_form($form, $form_state) {
    $query = db_select('views_view', 'v');
    $query->fields('v', array('name', 'human_name'));
    $exe_result = $query->execute();
    $result = $exe_result->fetchAll();
    $count = $exe_result->rowCount();

    $saved_options = variable_get('view_name');

    $views = array();
    if($count > 0){
    foreach ($result as $value) {
        $views[$value->name] = $value->human_name;
    }
    $form['view_name'] = array(
        '#type' => 'checkboxes',
        '#options' => $views,
        '#default_value' => $saved_options ? $saved_options : '',
    );
    return system_settings_form($form);
    }else{
        $form['view_name'] = array(
        '#markup' => 'No views found to configure.',
        
    );
        return $form;
    }
    
}
